#ifndef BALLS_THE_GAME_PHYSICS
#define BALLS_THE_GAME_PHYSICS

#include "util.cpp"
#include <cmath>

    static float distance(float x1, float y1, float x2, float y2) {
        return std::sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }
    class Physics {
    public:
        static void CalculateGameStates(const State& gameState,float time, id_t steps,
                                                             BlockingQueue<State>& states) {
            float stepTime = time / steps;
            State state = gameState;
            for (id_t step = 0; step < steps; ++step) {
                CalculateNextGameState(state, stepTime);
                states.push(state);
            }
        }
        static State CalculateLastGameState(const State& gameState,float time, id_t steps,
                                                             BlockingQueue<State>& states) {
            float stepTime = time / steps;
            State state = gameState;
            for (id_t step = 0; step < steps; ++step) {
                CalculateNextGameState(state, stepTime);
            }
            //states.push(state);
            return state;
        }
        static void CalculateNextGameState(State& gameState, float dt) {
            ++gameState.stateId;
            // сдвинуть чуваков
            for (auto& player: gameState.players) {
                player->x += player->vX * dt + player->aX * dt * dt / 2;
                player->y += player->vY * dt + player->aY * dt * dt / 2; 

                player->vX += player->aX * dt;
                player->vY += player->aY * dt;

                auto length = std::sqrt(player->vX * player->vX + player->vY * player->vY);
                if (length > gameState.velocityMax) {
                    player->vX *= gameState.velocityMax / length;
                    player->vY *= gameState.velocityMax / length;
                }
            }
            // собрать монетки
            CollectCoins(gameState);
            // обработка столкновений
            AnalizeCollisions(gameState);
        }

    private:
        static void CollectCoins(State& state) {
            std::vector< std::shared_ptr<Coin> > coins;
            for (auto& coin : state.coins) {

                std::vector<size_t> playersForCoinsIndexes;
                size_t index = 0;
                for (auto& player: state.players) {
                    if (distance(player->x, player->y, coin->x, coin->y) <
                        state.playerRadius + state.coinRadius) {
                            
                        playersForCoinsIndexes.push_back(index);
                    }
                    ++index;
                }
                if (playersForCoinsIndexes.empty()) {
                    coins.push_back(coin);
                    continue;
                }

                float gain = coin->value / playersForCoinsIndexes.size();
                for (size_t index: playersForCoinsIndexes) {
                    state.players[index]->score += gain;
                }                
            }
            state.coins = coins;
        }

        static bool SphereCollision(const State& state, const Player& left, const Player& right, float * nx, float * ny) {
            if (distance(left.x, left.y, right.x, right.y) > 2 * state.playerRadius)
                return false;

            float dx = -left.x + right.x;
            float dy = -left.y + right.y;

            float length = std::sqrt(dx * dx + dy * dy);
            dx /= length; dy /= length;

            *nx = dx; *ny = dy;
            return true;
        }

        static float Dot(float x1, float y1, float x2, float y2) {
            return x1 * x2 + y1 *y2;
        }

        static void AnalizeCollisions(State& state) {
            for (auto self : state.players) {

                if (distance(self->x, self->y, 0, 0) > state.fieldRadius - state.playerRadius) {

                    float nx = -self->x;
                    float ny = -self->y;

                    float length = std::sqrt(nx*nx + ny*ny);
                    nx /= length; ny /= length;

                    float alongNormal = -Dot(-self->vX, -self->vY, nx, ny);

                    float ix = alongNormal * nx * 2;
                    float iy = alongNormal * ny * 2;

                    self->vX -= ix; self->vY -= iy;
                }

                for (auto other : state.players) {
                    
                    if (self->id == other->id)
                        continue;
                    
                    float nx, ny;
                    if (SphereCollision(state, *self, *other, &nx, &ny)) {
                        float rvx = -self->vX + other->vX;
                        float rvy = -self->vY + other->vY;
                        float alongNormal = -Dot(rvx, rvy, nx, ny);
                        
                        if (alongNormal < 0)
                            continue;

                        float ix = alongNormal * nx;
                        float iy = alongNormal * ny;
                        
                        self->vX -= ix; self->vY -= iy;
                        other->vX += ix; other->vY += iy;
                    }
                } 
            }

        }    
    };


#endif
