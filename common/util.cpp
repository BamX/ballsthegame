#include <condition_variable>
#include <queue>
#include <memory>
#include <mutex>
#include <sstream>

#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"

#ifndef BALLS_THE_GAME_UTILL
#define BALLS_THE_GAME_UTILL

typedef std::vector<char> Buffer;
void appendTo (Buffer *buffer,const char *begin, size_t size);

   
    typedef unsigned int id_t;
    typedef std::vector<char> Buffer;

    class StringStream {
    private:
        std::stringstream _sstream;
    public:
        void Put(char ch) {
            _sstream << ch;
        }
        std::string GetStr() {
            return _sstream.str();
        }
    };

    struct Player {
        Player(id_t _id):id(_id){
            x = 0; y=0;
            vX =0; vY=0;
            score = 0 ; 
            aX =0; aY =0 ;
        }
        id_t id;
        float x, y;
        float vX, vY;
        float score;

        float aX, aY;
    };

    struct Coin {
        Coin(){}
        Coin(float _x,float _y, float _val):x(_x),y(_y),value(_val){}
        float x, y;
        float value;
    };

    struct State {
        id_t stateId;
        float fieldRadius, playerRadius, coinRadius;
        float velocityMax;
        float timeDelta;

        std::vector< std::shared_ptr<Player> > players;
        std::vector< std::shared_ptr<Coin> > coins;

        State(){
            stateId=0;
            fieldRadius= 150.0;
            playerRadius=10.0;
            coinRadius =5.0;
            velocityMax =100.0;
            timeDelta =1.05;

        }

        State(const State & other) {
            stateId = other.stateId;
            fieldRadius = other.fieldRadius;
            playerRadius = other.playerRadius;
            coinRadius = other.coinRadius;
            velocityMax = other.velocityMax;
            timeDelta = other.timeDelta;

            for (auto& player: other.players) {
                players.push_back(std::shared_ptr<Player>(new Player(*player)));
            }

            for (auto& coin: other.coins) {
                coins.push_back(std::shared_ptr<Coin>(new Coin(*coin)));
            }
        }

        Buffer serialize() {
            rapidjson::Document doc;
            doc.SetObject();
            doc.AddMember("type", "STATE", doc.GetAllocator());
            doc.AddMember("state_id", stateId, doc.GetAllocator());
            doc.AddMember("field_radius", fieldRadius, doc.GetAllocator());
            doc.AddMember("player_radius", playerRadius, doc.GetAllocator());
            doc.AddMember("coin_radius", coinRadius, doc.GetAllocator());
            doc.AddMember("velocity_max", velocityMax, doc.GetAllocator());
            doc.AddMember("time_delta", timeDelta, doc.GetAllocator());

            rapidjson::Value playersValue;
            playersValue.SetArray();
            for (auto& player: players) {
                rapidjson::Value playerValue;
                playerValue.SetObject();
                playerValue.AddMember("id", player->id, doc.GetAllocator());
                playerValue.AddMember("x", player->x, doc.GetAllocator());
                playerValue.AddMember("y", player->y, doc.GetAllocator());
                playerValue.AddMember("v_x", player->vX, doc.GetAllocator());
                playerValue.AddMember("v_y", player->vY, doc.GetAllocator());
                playerValue.AddMember("a_x", player->aX, doc.GetAllocator());
                playerValue.AddMember("a_y", player->aY, doc.GetAllocator());
                playerValue.AddMember("score", player->score, doc.GetAllocator());

                playersValue.PushBack(playerValue, doc.GetAllocator());
            }
            doc.AddMember("players", playersValue, doc.GetAllocator());

            rapidjson::Value coinsValue;
            coinsValue.SetArray();
            for (auto& coin: coins) {
                rapidjson::Value coinValue;
                coinValue.SetObject();
                coinValue.AddMember("x", coin->x, doc.GetAllocator());
                coinValue.AddMember("y", coin->y, doc.GetAllocator());
                coinValue.AddMember("value", coin->value, doc.GetAllocator());

                coinsValue.PushBack(coinValue, doc.GetAllocator());
            }
            doc.AddMember("coins", coinsValue, doc.GetAllocator());

            StringStream ss;
            rapidjson::PrettyWriter<StringStream> writer(ss);
            doc.Accept(writer);
            
            Buffer b;
            std::string str = ss.GetStr();
            appendTo(&b,str.data(),str.length());
            b.push_back(0);
            return b;
        }

        static std::shared_ptr<State> deserialize(Buffer &msg) {
            rapidjson::Document doc;
            doc.Parse<0>(msg.data());

            std::shared_ptr<State> state = std::shared_ptr<State>(new State);

            state->stateId = doc["state_id"].GetUint();
            state->fieldRadius = static_cast<float>(doc["field_radius"].GetDouble());
            state->playerRadius = static_cast<float>(doc["player_radius"].GetDouble());
            state->coinRadius = static_cast<float>(doc["coin_radius"].GetDouble());
            state->velocityMax = static_cast<float>(doc["velocity_max"].GetDouble());
            state->timeDelta = static_cast<float>(doc["time_delta"].GetDouble());

            const rapidjson::Value& playersValue = doc["players"];
            for (int i = 0; i < playersValue.Size(); ++i) {
                std::shared_ptr<Player> player = std::shared_ptr<Player>(new Player(playersValue[i]["id"].GetUint()));
                player->x = static_cast<float>(playersValue[i]["x"].GetDouble());
                player->y = static_cast<float>(playersValue[i]["y"].GetDouble());
                player->vX = static_cast<float>(playersValue[i]["v_x"].GetDouble());
                player->vY = static_cast<float>(playersValue[i]["v_y"].GetDouble());
                player->aX = static_cast<float>(playersValue[i]["a_x"].GetDouble());
                player->aY = static_cast<float>(playersValue[i]["a_y"].GetDouble());
                player->score = static_cast<float>(playersValue[i]["score"].GetDouble());

                state->players.push_back(player);
            }

            const rapidjson::Value& coinsValue = doc["coins"];
            for (int i = 0; i < coinsValue.Size(); ++i) {
                std::shared_ptr<Coin> coin = std::shared_ptr<Coin>(new Coin);
                coin->x = static_cast<float>(coinsValue[i]["x"].GetDouble());
                coin->y = static_cast<float>(coinsValue[i]["y"].GetDouble());
                coin->value = static_cast<float>(coinsValue[i]["value"].GetDouble());

                state->coins.push_back(coin);
            }

            return state;
        }
    };

    struct Turn {
        id_t stateId;     // id состояния, на которое дается ответ
        id_t id;          // id клиента
        float aX, aY;       // ускорения

        Turn() {
            stateId = 0;
            id = 0;
            aX = 0; aY = 0;
        }

        Buffer serialize() {
            rapidjson::Document doc;
            doc.SetObject();
            doc.AddMember("type", "TURN", doc.GetAllocator());
            doc.AddMember("state_id", stateId, doc.GetAllocator());
            doc.AddMember("id", id, doc.GetAllocator());
            doc.AddMember("a_x", aX, doc.GetAllocator());
            doc.AddMember("a_y", aY, doc.GetAllocator());

            StringStream ss;
            rapidjson::PrettyWriter<StringStream> writer(ss);
            doc.Accept(writer);
            Buffer b;
            std::string str = ss.GetStr();
            appendTo(&b,str.data(),str.length());
            b.push_back(0);
            return b;
        }
        static std::shared_ptr<Turn> deserialize(Buffer &msg) {
            rapidjson::Document doc;
            doc.Parse<0>(msg.data());

            std::shared_ptr<Turn> turn = std::shared_ptr<Turn>(new Turn);

            turn->stateId = doc["state_id"].GetUint();
            turn->id = doc["id"].GetUint();
            turn->aX = static_cast<float>(doc["a_x"].GetDouble());
            turn->aY = static_cast<float>(doc["a_y"].GetDouble());

            return turn;
        }
    };


    struct Hello{
        std::string type;
        Hello(){}
        Hello(int _type){
            if(_type == 1 ){
                type = "CLI_SUB_REQUEST";
            }else{
                type = "VIEW_SUB_REQUEST";
            }
        }
        bool isActive(){
            return type == "CLI_SUB_REQUEST";
        }
        Buffer serialize(){
            rapidjson::Document doc;
            doc.SetObject();
            doc.AddMember("type", type.data(), doc.GetAllocator());
            StringStream ss;
            rapidjson::PrettyWriter<StringStream> writer(ss);
            doc.Accept(writer);
            Buffer b;
            std::string str = ss.GetStr();
            appendTo(&b,str.data(),str.length());
            return b;
        }

        static std::shared_ptr<Hello> deserialize(Buffer &msg){
            rapidjson::Document doc;
            doc.Parse<0>(msg.data());
            std::shared_ptr<Hello> result = std::shared_ptr<Hello>(new Hello);
            result->type = doc["type"].GetString();
            return result;
        }

    };

    struct HelloAnswer{
        std::string type;
        std::string result;
        unsigned int id;
        HelloAnswer(){};
        HelloAnswer(bool flag,bool res, id_t _id): id(_id){
            if(flag == 1){
                type ="CLI_SUB_RESULT";
            } else {
                type = "VIEW_SUB_RESULT" ;
            }
            if(res == 1)
                result = "ok";
            else result = "fail";
        }
        bool isOk(){
            return result.length()==2;
        }
        Buffer serialize(){
            rapidjson::Document doc;
            doc.SetObject();
            doc.AddMember("type", type.data(), doc.GetAllocator());
            doc.AddMember("result", result.data(), doc.GetAllocator());
            doc.AddMember("id", id, doc.GetAllocator());
            StringStream ss;
            rapidjson::PrettyWriter<StringStream> writer(ss);
            doc.Accept(writer);
            Buffer b;
            std::string str = ss.GetStr();
            appendTo(&b,str.data(),str.length());
            return b;
        }
        
        static std::shared_ptr<HelloAnswer> deserialize(Buffer &msg){
            rapidjson::Document doc;
            doc.Parse<0>(msg.data());
            std::shared_ptr<HelloAnswer> result = std::shared_ptr<HelloAnswer>(new HelloAnswer);
            result->type = doc["type"].GetString();
            result->result = doc["result"].GetString();
            result->id = doc["id"].GetUint();
            return result;
        }
    };

#ifndef _GLIBCXX_HAS_GTHREADS
    // Dummy for mingw
    template<typename T>
    class BlockingQueue{
    private:
        std::queue<std::shared_ptr<T> > data_queue;
    public: 
        BlockingQueue(){}   
        void push(T new_value){
            std::shared_ptr<T> in(std::make_shared<T>(new_value));
            data_queue.push(in);
        }
        std::shared_ptr<T> wait_and_pop(){
            std::shared_ptr<T> res = data_queue.front();
            data_queue.pop();
            return res;
        }
        bool nonBlockingIsEmpty(){
            return data_queue.empty();
        }
    };
#else
    template<typename T>
    class BlockingQueue{
    private:
        std::mutex mut;
        std::condition_variable cond;
        std::queue<std::shared_ptr<T> > data_queue;
    public: 
        BlockingQueue(){}   
        void push(T new_value){
            std::shared_ptr<T> in(std::make_shared<T>(new_value));
            std::lock_guard<std::mutex> _lock(mut);
            data_queue.push(in);
            cond.notify_one();
        }
        std::shared_ptr<T> wait_and_pop(){
            std::unique_lock<std::mutex> _lock(mut);
            cond.wait(_lock, [this]{ return !data_queue.empty();});
            std::shared_ptr<T> res =data_queue.front(); 
            data_queue.pop();
            return res;
        }
        bool nonBlockingIsEmpty(){
            return data_queue.empty();
        }
    };
#endif

    int bufferToInt(Buffer & b);

    Buffer intToBuffer(int var,int len);



#endif
