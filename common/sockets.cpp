#ifndef SOCKETS_H
#define SOCKETS_H
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdexcept>
#include <vector>
#include <memory>
#include <iostream>

#include "../common/util.cpp"

bool socketDebug = true;
 
class OkOrException {
private:
   bool is_good_;
   std::runtime_error exception_;
 
public:
   OkOrException() :
       is_good_(true),
       exception_(std::runtime_error(""))
       {}
 
   void setException(const std::runtime_error &exception) {
       is_good_ = false;
       exception_ = exception;
   }
 
   void check() const {
       if (!is_good_) {
           throw exception_;
       }
   }
};
 
typedef OkOrException Status;
 
struct InternetAddress {
private:
   std::string address_;
   int port_;
 
public:
   explicit InternetAddress(std::string address, int port) :
       address_(address),
       port_(port)
       {}
 
   struct sockaddr getSocketAddress() const {
       struct sockaddr_in address;
       address.sin_family = AF_INET;
       address.sin_port = htons(port_);
       address.sin_addr.s_addr = inet_addr(address_.c_str());
       memset(address.sin_zero, '\0', sizeof address.sin_zero);
       return *((sockaddr *) &address);
   }

   ~InternetAddress() {}
};
 
class ConnectionSocket {
private:
   int connection_fd_;
   Status status_;
   bool have_resources_;

  void initSocket() {
       connection_fd_ = ::socket(AF_INET, SOCK_STREAM, 0);
       if (connection_fd_ < 0) {
           throw std::runtime_error("Failed to create socket");
       }
   }
 
   void connectToAddress(const InternetAddress &internet_address) {
       struct sockaddr address = internet_address.getSocketAddress();
       int bound = ::connect(connection_fd_, (struct sockaddr *) &address, sizeof(address));
       if (bound == -1) {
           throw std::runtime_error("Failed to bind socket");
       }
   }
 
public:
 
   ConnectionSocket(int connection_fd) :
       connection_fd_(connection_fd),
       have_resources_(true)
       {}
 
   ConnectionSocket(const ConnectionSocket &) = delete;
   ConnectionSocket &operator=(const ConnectionSocket &) = delete;
   ConnectionSocket(ConnectionSocket &&other) {
       connection_fd_ = other.connection_fd_;
       status_ = std::move(other.status_);
       other.have_resources_ = false;
       have_resources_ = true;
   }



   ConnectionSocket(const InternetAddress &internet_address) {
       try {
           initSocket();
           connectToAddress(internet_address);
       } catch(const std::runtime_error &exception) {
           status_.setException(exception);
       }
    }
 
   Buffer read() {
       constexpr int BUFFER_SIZE = 1024;
       char buffer[BUFFER_SIZE];
 
       Buffer result;
 
       while (true) {
           memset(buffer, 0, BUFFER_SIZE);
           int recv_result = recv(connection_fd_, buffer, BUFFER_SIZE, 0);
           appendTo(&result, buffer, BUFFER_SIZE);
           
           if (recv_result < BUFFER_SIZE) {
               break;
           }
       }
       return std::move(result);
   }

   Buffer read(int length){
      constexpr int BUFFER_SIZE = 1024;
      char buffer[BUFFER_SIZE];
      Buffer result;
      int left = length;
      while(left > 0){
        memset(buffer, 0, BUFFER_SIZE);
        int recv_result = recv(connection_fd_, buffer, std::min(BUFFER_SIZE,left), 0);
        if(recv_result == 0 ){
          throw std::runtime_error("Looks like connection broken");
        }
        appendTo(&result, buffer, recv_result);
        left -= recv_result;
      }
      return result;
   }

   Buffer readMsg(){  
      Buffer buf =  this->read(4);
      int msgSize = bufferToInt(buf);
      buf = this->read(msgSize);
      if(socketDebug){
        std::cout<<std::endl << "read msg with size: "<<msgSize <<std::endl;
        for(int i =0; i<buf.size() ;++i){
          std::cout<<buf[i];
        }
        std::cout<<std::endl;
      }
      buf.push_back('\n');
      return buf;
   }
   void writeMsg(Buffer &b){
      Buffer f = intToBuffer(b.size(),4);
      if(socketDebug){
        std::cout<<std::endl<<"write msg: "<<std::endl ;
        for(int i =0; i < b.size(); ++i){
          std::cout<<b[i];
        }
        std::cout<<std::endl;
      }
      try {
        this->write(f);
        std::cout<< "now writing msg ..." << std::endl;
        this->write(b);
        std::cout<< "done" << std::endl;
      } catch(const std::runtime_error &exception) {
          std::cout<<"runtime_error cought"<<std::endl;
          status_.setException(exception);
      }
   }

   void write(Buffer &buffer) {
       int res = send(connection_fd_, buffer.data(), buffer.size(), 0);
       if(res != buffer.size()) {
           throw std::runtime_error("Failed to write to socket");
       } 
       //std::cout<<std::endl<< "write res "<< res<<std::endl;
   }
 
   ~ConnectionSocket() {
       if (have_resources_) {
           ::close(connection_fd_);
       }
   }
  void check() const {
       status_.check();
   }
};
 
class ServerSocket {
private:
   int fd_;
   Status status_;
 
   void initSocket() {
       fd_ = ::socket(AF_INET, SOCK_STREAM, 0);
       if (fd_ < 0) {
           throw std::runtime_error("Failed to create socket");
       }
   }
 
   void bindToAddress(const InternetAddress &internet_address) {
       struct sockaddr address = internet_address.getSocketAddress();
       int bound = ::bind(fd_, (struct sockaddr *) &address, sizeof(address));
       if (bound == -1) {
           throw std::runtime_error("Failed to bind socket");
       }
   }
 
   void listenTo() {
       int listen_result = ::listen(fd_, 10);
       if (listen_result == -1) {
           throw std::runtime_error("Failed to listen");
       }
   }
 
   void closeSocket() {
       ::close(fd_);
       //::unlink(fd_);
   }
 
public:
   ServerSocket(const InternetAddress &internet_address) {
       try {
           initSocket();
           bindToAddress(internet_address);
           listenTo();
       } catch(const std::runtime_error &exception) {
           status_.setException(exception);
       }
   }
 
   ConnectionSocket newConnection() {
       int new_fd = ::accept(fd_, 0, 0);
       if (new_fd < 0) {
           throw std::runtime_error("Failed to accept socket");
       }
       return std::move(ConnectionSocket(new_fd));
   }
 
   ServerSocket(const ServerSocket &) = delete;
   ServerSocket &operator=(const ServerSocket &) = delete;
 
   void check() const {
       status_.check();
   }
 
   ~ServerSocket() {
       closeSocket();
   }
};

#endif
