#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <atomic>
#include <memory>
#include <vector>
#include <list>
#include <set>
#include <cstdlib>
#include <ctime>
#include <string>
#include <condition_variable>

//код с семинара

class TTask {
    public:
        virtual ~TTask() {}
        virtual void Do(std::atomic<bool> &stopFlag) = 0;
};

class TThreadPool {
    private:
        typedef std::shared_ptr< std::thread > TThreadPtr;
        typedef std::shared_ptr< TTask > TTaskPtr;
        std::atomic<bool> StopFlag;
        std::vector<TThreadPtr> Workers;
        std::mutex Mutex;
        std::condition_variable Condition;
        std::list<TTaskPtr> Tasks;
        std::atomic<size_t> ActiveTasks;

        void DoTopTask(std::unique_lock<std::mutex> &lock) {
            TTaskPtr task = Tasks.front();
            Tasks.pop_front();
            ++ActiveTasks;
            lock.unlock();
            task->Do(StopFlag);
            if (--ActiveTasks == 0)
                Condition.notify_all();
        }

        void ThreadFunc() {
            while (!StopFlag) {
                std::unique_lock<std::mutex> lock(Mutex);
                while (!StopFlag && Tasks.empty())
                    Condition.wait(lock);
                if (!StopFlag && !Tasks.empty())
                    DoTopTask(lock);
            }
        }

    public:
        TThreadPool(size_t workersCount)
            : StopFlag(false)
            , ActiveTasks(0)
        {
            Workers.reserve(workersCount);
            for (size_t i = 0; i < workersCount; ++i)
                Workers.push_back(TThreadPtr(new std::thread([this]() { ThreadFunc(); })));
        }
        ~TThreadPool() {
            try {
                Stop(false);
            } catch (...) {
            }
        }
        void Stop(bool finishTasks) {
            StopFlag.store(true);
            Condition.notify_all();
            if (finishTasks)
                WaitAllTasks(); 
            for (auto w : Workers) {
                w->join();
            }
        }
        bool AddTask(const TTaskPtr &task) {
            if (StopFlag)
                return false;
            std::lock_guard<std::mutex> lock(Mutex);
            Tasks.push_back(task);
            Condition.notify_one();
            return true;
        }
        bool WaitAllTasks() {
            std::unique_lock<std::mutex> lock(Mutex);
            while (!StopFlag && (!Tasks.empty() || ActiveTasks)) {
                Condition.wait(lock);
            }
            return !StopFlag;
        }
};


class TEchoTask : public TTask {
    public:
        virtual void Do(std::atomic<bool> &) {
            static std::mutex m;
            static std::set<std::thread::id> Ids;
            std::lock_guard<std::mutex> lock(m);
            Ids.insert(std::this_thread::get_id());
            std::cout << "Hello from thread " << std::this_thread::get_id() << ", total number of worked thread is " << Ids.size() << std::endl;
        }
};


class TTimer {
    private:
        std::string Hint;
        time_t Start;

    public:
        TTimer(const std::string &hint)
            : Hint(hint)
            , Start(time(NULL))
        {
        }
        ~TTimer() {
            Stamp(Hint);
        }
        void Stamp(const std::string &hint) {
            time_t end = time(NULL);
            std::cout << hint << end - Start << std::endl;
        }
};
