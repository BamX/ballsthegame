#include <iomanip>        
#include <thread>         
#include <chrono>         
#include <ctime>          
#include <set>
#include <cmath>

#include "../common/thread_pool.cpp"
#include "../common/sockets.cpp"
#include "../common/physics.cpp"

#include "../client/util_cpp.cpp"

using std::chrono::system_clock;
class Server;

class LogItem{
public: 
	typedef std::shared_ptr<Buffer> Str;
	Str msg;
	LogItem(Buffer &_msg){
		msg = std::make_shared<Buffer>(std::move(_msg));
	}
};

class LoggingTask: public TTask{
private: 
	std::shared_ptr<BlockingQueue<LogItem> > queue;
public: 
	LoggingTask(std::shared_ptr<BlockingQueue<LogItem> > &_q){
		queue= _q;
	}
	virtual void Do(std::atomic<bool> &stopFlag) {
		//TODO: add sleep
		while(!stopFlag){
			if(queue->nonBlockingIsEmpty()){
				std::this_thread::sleep_for (std::chrono::milliseconds(100));
				continue;
			}
			std::shared_ptr<LogItem> item = queue->wait_and_pop();
			char * ptr = item->msg->data();
			for(int i =0 ; i < item->msg->size(); ++i){
				std::cout<< ptr[i];
			}
			std::cout << std::endl;
		} 
	}

};

class MakeTurnTask : public TTask{
private:
	std::shared_ptr<Server> server;
public:
	MakeTurnTask(std::shared_ptr<Server>  &_server){
		server= _server;
	}
	virtual void Do(std::atomic<bool> &stopFlag);

};

class ListenTask: public TTask{
private: 
	std::shared_ptr<ConnectionSocket> connection;
	std::shared_ptr<Server> server;
public: 
	ListenTask(std::shared_ptr<Server> &_server,std::shared_ptr<ConnectionSocket>  &_socket){
		connection= _socket;
		server = _server;
	}
	virtual void Do(std::atomic<bool> &stopFlag);
};



class Server{
private: 
	std::shared_ptr<TThreadPool> tPool;
	std::shared_ptr<ServerSocket> incomeSocket;
	std::shared_ptr<BlockingQueue<LogItem> > serverQueue;
	std::shared_ptr<Server> self;
	std::shared_ptr<std::vector<std::shared_ptr<Turn> > > prevTurn; 
	std::shared_ptr<std::vector<std::shared_ptr<Turn> > > curTurn;
    std::mutex registrationMutex;
	bool stop;
	id_t clientNumber;
public: 
	std::vector<std::shared_ptr<ConnectionSocket> > clients; 
	std::shared_ptr<struct std::tm> serverTime;
	id_t stateID;
	State state;

	Server(InternetAddress &address,int threadPoolCapacity){
		tPool  = std::shared_ptr<TThreadPool>(new TThreadPool(threadPoolCapacity));
		incomeSocket = std::shared_ptr<ServerSocket>(new ServerSocket(address));
		serverQueue = std::shared_ptr<BlockingQueue<LogItem> > (new BlockingQueue<LogItem>());
		self = std::shared_ptr<Server>(this);
		prevTurn = std::shared_ptr<std::vector<std::shared_ptr<Turn>  > >(new std::vector<std::shared_ptr<Turn>  >());
		curTurn = std::shared_ptr<std::vector<std::shared_ptr<Turn>  > >(new std::vector<std::shared_ptr<Turn>  >());


		stop = false;
		stateID = 0; 
  		std::time_t tt = system_clock::to_time_t (system_clock::now());
  		serverTime = std::shared_ptr<struct std::tm>(std::localtime(&tt));
  		clientNumber = 0;
  		tPool->AddTask(std::shared_ptr<LoggingTask>(new LoggingTask(serverQueue)));
  		tPool->AddTask(std::shared_ptr<MakeTurnTask>(new MakeTurnTask(self)));
	}

	void runEventLoop(){
		incomeSocket->check();
		while(!stop){
			std::shared_ptr<ConnectionSocket> connection_socket(new ConnectionSocket(incomeSocket->newConnection()));
			clients.push_back(connection_socket);
			tPool->AddTask(std::shared_ptr<ListenTask>(new ListenTask(self,connection_socket)));
		}
	}

	std::shared_ptr<std::vector<std::shared_ptr<Turn>  > > swapActiveTurns(){
		std::lock_guard<std::mutex> _lock(registrationMutex);
  		std::swap(curTurn,prevTurn);
  		return prevTurn;
	}

	std::shared_ptr<std::vector<std::shared_ptr<Turn>  > > getTurn(){
		return curTurn;
	}

	id_t registerClient(bool active){
		std::lock_guard<std::mutex> _lock(registrationMutex);
		++clientNumber;
		curTurn->push_back(std::shared_ptr<Turn>(new Turn));
		prevTurn->push_back(std::shared_ptr<Turn>(new Turn));
		if(active) state.players.push_back(std::shared_ptr<Player> (new Player(clientNumber-1)));
		return (clientNumber-1);
	}
	void generateCoin(float probe){
		float r = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/1.0));
		if(r > probe) return;
		float radius = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX)) * (state.fieldRadius - state.coinRadius);
		float alpha = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX)) * 2 * 3.14159265f;
		float val = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX)) * 10;

		float x = radius * cos(alpha);
		float y = radius * sin(alpha);
		state.coins.push_back(std::shared_ptr<Coin> (new Coin(x,y,val)));

	}
	~Server(){
		stop = true;
		//dont wait for tasks 
		tPool->Stop(false);
	}
};

void  MakeTurnTask::Do(std::atomic<bool> &stopFlag) {
 	while(!stopFlag){
  			++server->serverTime->tm_sec;
  			std::this_thread::sleep_until (system_clock::from_time_t (mktime(server->serverTime.get())));
  			++server->stateID;
  			++(server->state.stateId);
  			std::cout<<"turn " <<server->stateID <<" begins" <<std::endl;
  			std::shared_ptr<std::vector<std::shared_ptr<Turn>  > > turn =   server->swapActiveTurns();
  			//TODO: low priority. make normal 
  			for(std::vector<std::shared_ptr<Turn>  >::iterator it = turn->begin(); it != turn->end(); ++it){
  				//if(it->stateId != server->stateID-1) continue;
  				for(std::vector< std::shared_ptr<Player> >:: iterator palyersIter = server->state.players.begin();
  					palyersIter != server->state.players.end(); ++palyersIter){

  					std::cout<<"turn iter"<<(*it)->id << " "<< (*palyersIter)->id<< std::endl;
  					if((*it)->id == (*palyersIter)->id){
  						(*palyersIter)->aX = (*it)->aX;
  						(*palyersIter)->aY = (*it)->aY;
  					}
  				}
  			}
  			
  			BlockingQueue<State>  strangeQueue;
			server->state = Physics::CalculateLastGameState(server->state, 1.0, 1000, strangeQueue);
			for(int i =0; i < 1; ++i){
				server->generateCoin(0.2);
			}

			//TODO:call phisics,convert state to buffer
  			Buffer resp = server->state.serialize();
  			for(std::vector<std::shared_ptr<ConnectionSocket> >:: iterator it = server->clients.begin();
  			 	it !=  server->clients.end(); ++it){
  				std::cout<<"in send loop" << std::endl;
  				try {
  					(*it)->check();
  					(*it)->writeMsg(resp);
  				} catch (const std::exception &ex) {
			       std::cout << ex.what() << std::endl;
   				}
  			}
		}
	} 
void  ListenTask::Do(std::atomic<bool> &stopFlag) {
	Buffer msg = connection->readMsg();
	for(int i = 0; i < msg.size(); ++i ){
		std::cout<<msg[i];
	}
	std::cout<<std::endl;
	std::shared_ptr<Hello> h = Hello::deserialize(msg);
	std::cout<<"deserialized" <<std::endl;
	bool isActive = h->isActive();
	id_t clientId = server->registerClient(isActive);
	HelloAnswer ha(isActive,1,clientId);
	msg = ha.serialize();
	connection->writeMsg(msg);
	if(!isActive) return;
	try {
		while(!stopFlag){
			msg = connection->readMsg();
			std::shared_ptr<Turn>  sp= Turn::deserialize(msg);
			std::shared_ptr<std::vector<std::shared_ptr<Turn>  > > turn = server->getTurn();
			if (sp->stateId >= (*turn)[clientId]->stateId)
				(*turn)[clientId] = sp;
		} 
	} catch (const std::exception &ex) {
		std::cout << ex.what() << std::endl;
   	}
}

int main(int argsize,char ** argv){
	static constexpr char LOCALHOST[] = "127.0.0.1";
    static constexpr int DEFAULT_PORT = 8111;
    int port = DEFAULT_PORT;
    if(argsize > 1) port = atoi(argv[1]);
	InternetAddress address = InternetAddress(LOCALHOST, port);
	Server server (address,128);

    std::cout<<  "running on port " << port << std:: endl;
	try {
       server.runEventLoop();
   	} catch (const std::exception &ex) {
       std::cerr << ex.what() << std::endl;
       return 1;
   	}
	return 0; 
}