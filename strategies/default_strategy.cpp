#include <vector>
#include <string>
#include <memory>
#include <cmath>
#include <iostream>

#include <cstdio>

#include "../common/util.cpp"
#include "../common/physics.cpp"


    class Strategy {
    protected:
        std::shared_ptr<State> currentState;
        std::shared_ptr<Turn> currentTurn;

        size_t localPlayerId;

        std::shared_ptr<Player> getLocalPlayer() {
            for (auto& player: currentState->players) {
                if (player->id == localPlayerId) {
                    return player;
                }
            }
            return std::shared_ptr<Player>(NULL);
        }

        void setTurnFromTo(float fromX, float fromY, float toX, float toY) {
            float dx = toX - fromX,
                dy = toY - fromY;
            float mean = sqrt(dx * dx + dy * dy);
            currentTurn->aX = dx / mean;
            currentTurn->aY = dy / mean;
        }

        virtual void calcTurn() = 0;
    public:
        Strategy(id_t clientId) {
            localPlayerId = clientId;
        }

        std::shared_ptr<Turn> Do(std::shared_ptr<State> state) {
            currentState = state;
            currentTurn = std::shared_ptr<Turn>(new Turn);
            currentTurn->stateId = currentState->stateId;
            currentTurn->id = localPlayerId;

            calcTurn();

            return currentTurn;
        }
    };

    class SimpleAIStrategy : public Strategy {
    protected:
        virtual float coinWeight(std::shared_ptr<Coin> coin, std::shared_ptr<Player> player) {
            float dx = player->x - coin->x,
                dy = player->y - coin->y;
            return coin->value / (dx * dx + dy * dy);
        }

        std::shared_ptr<Coin> getNearestCoin(std::shared_ptr<Player> player) {
            if (currentState->coins.size() == 0) {
                return std::shared_ptr<Coin>(NULL);
            }

            std::shared_ptr<Coin> minCoin = currentState->coins[0];
            float minWeight = 0;
            for (auto& coin: currentState->coins) {
                float weight = coinWeight(coin, player);
                if (weight > minWeight) {
                    minCoin = coin;
                    minWeight = weight;
                }
            }
            return minCoin;
        }

        virtual void idleAction() {
            std::shared_ptr<Player> localPlayer = getLocalPlayer();
            setTurnFromTo(localPlayer->x, localPlayer->y, 
                currentState->fieldRadius, currentState->fieldRadius);
        }

        void flyToNearestCoin() {
            if (currentState->coins.size() == 0) {
                idleAction();
                return;
            }

            std::shared_ptr<Player> currentPlayer = getLocalPlayer();
            std::shared_ptr<Coin> targetCoin = getNearestCoin(currentPlayer);
            setTurnFromTo(currentPlayer->x, currentPlayer->y,
                targetCoin->x, targetCoin->y);

            for (auto& player: currentState->players) {
                if (true/*player cross target line*/) {
                    //TODO: fly around - turn -V_other * radius
                }
            }
        }

        virtual void calcTurn() {
            flyToNearestCoin();
        }
    public:
        SimpleAIStrategy(id_t clientId) 
            : Strategy(clientId) 
        { }
    };

    class BrutforceAIStrategy : public Strategy {
    protected:
        virtual float coinWeight(std::shared_ptr<Coin> coin, std::shared_ptr<Player> player) {
            float dx = player->x - coin->x,
                dy = player->y - coin->y;
            return coin->value / sqrt(dx * dx + dy * dy);
        }

        float stateScore(std::shared_ptr<State> state) {
            float score = 0;
            std::shared_ptr<Player> localPlayer;
            for (auto& player: state->players) {
                if (player->id == localPlayerId) {
                    score += player->score;
                    localPlayer = player;
                }
            }
            for (auto& coin: state->coins) {
                score += coinWeight(coin, localPlayer);
            }
            return score;
        }

        virtual void calcTurn() {
            const int divCount = 6;
            const int deepCount = 10;

            float curDeltaAngle = 2 * 3.14159265f / divCount;
            float curAngle = 0;
            float nextAngle = 0;
            BlockingQueue<State> newStates;

            float bestScore = 0;
            std::shared_ptr<State> bestState = currentState;

            for (int i = 0; i < deepCount; ++i) {
                for (int j = 0; j < divCount; ++j) {
                    curAngle += curDeltaAngle;

                    State st = *currentState;
                    for (auto& player: st.players) {
                        if (player->id == localPlayerId) {
                            player->aX = cos(curAngle);
                            player->aY = sin(curAngle);
                        }
                    }

                    std::shared_ptr<State> newState = std::shared_ptr<State>(new State(Physics::CalculateLastGameState(st, 10, 1000, newStates)));

                    float score = stateScore(newState);
                    if (score > bestScore) {
                        bestScore = score;
                        bestState = newState;
                        nextAngle = curAngle;
                    }
                }
                curAngle = nextAngle - curDeltaAngle / 2;
                curDeltaAngle /= divCount;
            }

            std::cout << "Angle: " << curAngle << "\nScore: " << bestScore << "\n";
            //std::cout << "Best state finish: " << bestState->serialize().data() << "\n";

            for (auto& player: bestState->players) {
                if (player->id == localPlayerId) {
                    currentTurn->aX = player->aX;
                    currentTurn->aY = player->aY;
                }
            }
        }
    public:
        BrutforceAIStrategy(id_t clientId) 
            : Strategy(clientId) 
        { }
    };

/*
int main() {
    //auto state = ballsthegame::State::deserialize(std::string("{\"type\":\"STATE\",\"state_id\":1111,\"field_radius\":50,\"player_radius\":2,\"coin_radius\":1,\"time_delta\":100,\"velocity_max\":11,\"players\":[{\"id\":0,\"x\":0.2,\"y\":0.3,\"v_x\":0.5,\"v_y\":0.7,\"score\":10.4}],\"coins\":[{\"x\":0,\"y\":3.5,\"value\":7}]}"));

    //const char * str = "{\"type\":\"STATE\",\"state_id\":1111,\"field_radius\":-0.0000128792,\"player_radius\":-0.0000114967,\"coin_radius\":1.4013E-45,\"velocity_max\":4.02951E-34,\"time_delta\":1.4013E-45,\"players\":[{\"id\":0,\"x\":5.19304E+33,\"y\":2.59652E+33,\"v_x\":2.59652E+33,\"v_y\":1.4013E-45,\"score\":1.4013E-45}],\"coins\":[]}";
    //const char * str = "{\"type\":\"STATE\",\"state_id\":134519385,\"field_radius\":-0.0000137398,\"player_radius\":-0.0000123572,\"coin_radius\":2.8026E-45,\"velocity_max\":4.02996E-34,\"time_delta\":2.8026E-45,\"players\":[{\"id\":0,\"x\":5.8419E+33,\"y\":2.92104E+33,\"v_x\":2.92104E+33,\"v_y\":1.4013E-45,\"score\":1.4013E-45}],\"coins\":[]}";
    //const char * str = "{\"type\":\"STATE\",\"state_id\":134519385,\"field_radius\":-0.0000130655,\"player_radius\":-0.000011683,\"coin_radius\":2.8026E-45,\"velocity_max\":4.02999E-34,\"time_delta\":2.8026E-45,\"players\":[{\"id\":0,\"x\":0,\"y\":0,\"v_x\":0,\"v_y\":0,\"score\":0}],\"coins\":[]}";
    //const char * str = "{\"type\":\"STATE\",\"state_id\":258258,\"field_radius\":350,\"player_radius\":20,\"coin_radius\":10,\"velocity_max\":100,\"time_delta\":1.05,\"players\":[{\"id\":0,\"x\":183.685,\"y\":-63.0794,\"v_x\":8.5756,\"v_y\":-19.1958,\"a_x\":0.5,\"a_y\":0.866025,\"score\":10.716},{\"id\":1,\"x\":69.5395,\"y\":56.2241,\"v_x\":1.26788,\"v_y\":6.42734,\"a_x\":0.5,\"a_y\":0.866025,\"score\":12.8061},{\"id\":2,\"x\":94.553,\"y\":165.707,\"v_x\":-6.14845,\"v_y\":-10.4931,\"a_x\":0.5,\"a_y\":0.866025,\"score\":0.0}],\"coins\":[{\"x\":-318.475,\"y\":-52.4853,\"value\":0.860558},{\"x\":135.357,\"y\":-61.798,\"value\":9.3081},{\"x\":305.6,\"y\":104.693,\"value\":5.21563},{\"x\":-45.4975,\"y\":-5.8087,\"value\":0.782321},{\"x\":-67.5484,\"y\":16.7023,\"value\":8.19677},{\"x\":265.941,\"y\":129.929,\"value\":0.0416161},{\"x\":8.26805,\"y\":-180.285,\"value\":3.04295},{\"x\":126.995,\"y\":-220.265,\"value\":9.25377},{\"x\":-95.5355,\"y\":-68.0273,\"value\":8.33243},{\"x\":-46.8593,\"y\":-132.725,\"value\":6.73937},{\"x\":63.5587,\"y\":-61.6826,\"value\":6.86125},{\"x\":-24.4081,\"y\":28.9073,\"value\":5.76691},{\"x\":-137.809,\"y\":245.246,\"value\":1.88201}]}";
    //const char * str = "{\"type\":\"STATE\",\"state_id\":258258,\"field_radius\":350,\"player_radius\":20,\"coin_radius\":10,\"velocity_max\":100,\"time_delta\":1.05,\"players\":[],\"coins\":[{\"x\":-318.475,\"y\":-52.4853,\"value\":0.860558},{\"x\":135.357,\"y\":-61.798,\"value\":9.3081},{\"x\":305.6,\"y\":104.693,\"value\":5.21563},{\"x\":-45.4975,\"y\":-5.8087,\"value\":0.782321},{\"x\":-67.5484,\"y\":16.7023,\"value\":8.19677},{\"x\":265.941,\"y\":129.929,\"value\":0.0416161},{\"x\":8.26805,\"y\":-180.285,\"value\":3.04295},{\"x\":126.995,\"y\":-220.265,\"value\":9.25377},{\"x\":-95.5355,\"y\":-68.0273,\"value\":8.33243},{\"x\":-46.8593,\"y\":-132.725,\"value\":6.73937},{\"x\":63.5587,\"y\":-61.6826,\"value\":6.86125},{\"x\":-24.4081,\"y\":28.9073,\"value\":5.76691},{\"x\":-137.809,\"y\":245.246,\"value\":1.88201}]}";
    //const char * str = "{\"type\":\"STATE\",\"state_id\":258258,\"field_radius\":350,\"player_radius\":20,\"coin_radius\":10,\"velocity_max\":100,\"time_delta\":1.05,\"players\":[],\"coins\":[]}";
    //const char * str = "{\"type\":\"STATE\",\"state_id\":258258,\"field_radius\":350,\"player_radius\":20,\"coin_radius\":10,\"velocity_max\":100,\"time_delta\":1.05,\"players\":[],\"coins\":[{\"x\":-318.475,\"y\":-52.4853,\"value\":0.860558},{\"x\":135.357,\"y\":-61.798,\"value\":9.3081},{\"x\":305.6,\"y\":104.693,\"value\":5.21563},{\"x\":-45.4975,\"y\":-5.8087,\"value\":0.782321},{\"x\":-67.5484,\"y\":16.7023,\"value\":8.19677},{\"x\":265.941,\"y\":129.929,\"value\":0.0416161},{\"x\":8.26805,\"y\":-180.285,\"value\":3.04295},{\"x\":126.995,\"y\":-220.265,\"value\":9.25377}]}";
    //const char * str = "{\"type\":\"STATE\",\"state_id\":258258,\"field_radius\":350,\"player_radius\":20,\"coin_radius\":10,\"velocity_max\":100,\"time_delta\":1.05,\"players\":[],\"coins\":[{\"x\":-318.475,\"y\":-52.4853,\"value\":0.860558}]}";
    const char * str = "{\"type\":\"STATE\",\"state_id\":258258,\"field_radius\":350,\"player_radius\":20,\"coin_radius\":10,\"velocity_max\":100,\"time_delta\":1.05,\"players\":[{\"id\":0,\"x\":183.685,\"y\":-63.0794,\"v_x\":8.5756,\"v_y\":-19.1958,\"a_x\":0.5,\"a_y\":0.866025,\"score\":10.716}],\"coins\":[{\"x\":-318.475,\"y\":-52.4853,\"value\":0.860558}]}";
    Buffer input;
    appendTo(&input, str, strlen(str));

    auto state = State::deserialize(input);

    std::cout << state->serialize().data() << "\n";

    BrutforceAIStrategy bst(0);
    auto turn = bst.Do(state);

    std::cout << turn->serialize().data() << "\n";

    return 0;
}*/
