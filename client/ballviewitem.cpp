#include "ballviewitem.h"

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

#include <iostream>

BallViewItem::BallViewItem(int id, float radius) :
      id(id), radius(radius),
      color(qrand() % 256, qrand() % 256, qrand() % 256) {
}


QRectF BallViewItem::boundingRect() const {
    return QRectF(-radius, -radius, 2 * radius, 2 * radius);
}

void BallViewItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *) {
    painter->setBrush(color);
    painter->drawEllipse(-radius, -radius, 2 * radius, 2 * radius);
    QTextOption opt = QTextOption();
    opt.setAlignment(Qt::AlignCenter);
    painter->drawText(QRectF(-radius, -radius, 2 * radius, 2 * radius),
                      QString::number(id), opt);
}

void BallViewItem::advance(int step) {
    if(!step) return;

    setPos(new_x, new_y);
}

float BallViewItem::getX() {
    return new_x;
}

float BallViewItem::getY() {
    return new_y;
}

void BallViewItem::setX(float nx) {
    new_x = nx;
}

void BallViewItem::setY(float ny) {
    new_y = ny;
}
