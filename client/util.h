#include <vector>
#include <memory>

typedef unsigned int id_t;

struct Player {
    id_t id;
    float x, y;
    float vX, vY;
    float score;
};

struct Coin {
    id_t id;
    float x, y;
    float value;
};

struct State {
    id_t stateId;
    float fieldRadius, playerRadius, coinRadius;
    float velocityMax;
    float timeDelta;

    std::vector< std::shared_ptr<Player> > players;
    std::vector< std::shared_ptr<Coin> > coins;
};
