#include <vector>
#include <sstream>

typedef std::vector<char> Buffer;


    int bufferToInt(Buffer & b) {
        int res = 0;
        int mult = 1;
        for(int i =b.size()-1; i>=0 ;--i ){
            res+=mult*(int)b[i];
            mult *=10;
        }
        return res;
    }

    Buffer intToBuffer(int var,int len) {
        Buffer b(len);
        for(int i= len-1; i>=0;--i){
            b[i]=var%10;
            var=var/10;
        }
        return b;
    }

void appendTo (Buffer *buffer,const char *begin, size_t size) {
   buffer->insert(buffer->end(), begin, begin + size);
}
