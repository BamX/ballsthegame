#ifndef GAMEVIEWWINDOW_H
#define GAMEVIEWWINDOW_H

#include "../common/util.cpp"
#include "scoreviewitem.h"
#include <atomic>
#include <QGraphicsScene>

class GameViewWindow {
public:
    GameViewWindow(int argc, char *argv[], int w, int h);
    bool isWinOpened();
    void changeState(const State& state);

private:
    std::atomic<bool> win_opened;
    std::atomic<float> field_radius;
    QGraphicsScene* scene_ptr;
    QGraphicsEllipseItem* arena;
    ScoreViewItem* text_item;
    void startWindow(int argc, char *argv[], int w, int h);
};

#endif // GAMEVIEWWINDOW_H
