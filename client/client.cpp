#include "gameviewwindow.h"
#include <QApplication>
#include <QtWidgets>
#include <iostream>
#include <chrono>
#include <thread>
#include <memory>
#include "../common/thread_pool.cpp"
#include "../common/sockets.cpp"
#include "../common/physics.cpp"
#include "../strategies/default_strategy.cpp"
#include "../common/util.cpp"


class Client;

class ListenTask : public TTask{
private:
	Client *client;
public:
	ListenTask(Client *_client){
		client= _client;
	}
	virtual void Do(std::atomic<bool> &stopFlag);

};

class ReactTask : public TTask{
private:
	std::shared_ptr<BrutforceAIStrategy> bst;
	Client *client;
	id_t stateId;
public:
	ReactTask(Client *_client, id_t _stateId,id_t client_id){
		client= _client;
		stateId = _stateId;
		bst = std::shared_ptr<BrutforceAIStrategy>(new BrutforceAIStrategy(client_id));
		std::cout<<"ReactTask with id = " << client_id<<std::endl;
	}
	virtual void Do(std::atomic<bool> &stopFlag);
};

class GuiTask : public TTask{
private:
	std::shared_ptr<BlockingQueue<State> > guiQueue;
public:
	GuiTask(std::shared_ptr<BlockingQueue<State> > _guiQueue){
		guiQueue= _guiQueue;
	}
	virtual void Do(std::atomic<bool> &stopFlag);
};

class Client{
private:
	std::shared_ptr<TThreadPool> tPool;
	std::shared_ptr<State> state;
	int clientType;
public:
	std::shared_ptr<BlockingQueue<State> > guiQueue;
	std::shared_ptr<ConnectionSocket> connection;

	Client(InternetAddress &address,int type){
		tPool  = std::shared_ptr<TThreadPool>(new TThreadPool(2));
		connection = std::shared_ptr<ConnectionSocket>(new ConnectionSocket(address));
		guiQueue = std::shared_ptr<BlockingQueue<State> > (new BlockingQueue<State>());

		state = std::shared_ptr<State>(new State());
		clientType = type;
		
	}

	void runEventLoop(){
		connection->check();

		Hello h(clientType);
		Buffer msg = h.serialize();
		std::cout<<"hello size" << msg.size();
		connection->writeMsg(msg); 

		msg = connection->readMsg();
		std::shared_ptr<HelloAnswer> ha = HelloAnswer::deserialize(msg);  	
		
		if(!ha->isOk()){
			std::cout<<"failed to register client" << std::endl;
			return;
		}
		tPool->AddTask(std::shared_ptr<ListenTask>(new ListenTask(this)));
		if(clientType == 1)
			tPool->AddTask(std::shared_ptr<ReactTask>(new ReactTask(this,state->stateId,ha->id)));
	}

	void callGuiUpdate(){
		//TODO low:without convertions 
        Physics::CalculateGameStates(*state.get(),1.0,100,*guiQueue.get());
	}

	void updateState(std::shared_ptr<State> &stateptr){
		state = stateptr;
	}
	bool checkState(id_t _stateId){
		return _stateId == state->stateId;
	}
	std::shared_ptr<State> getState(){
		return state;
	}
	void simulateState(float dt) {
        Physics::CalculateNextGameState(*state.get(), 1.0 / 60);
	}

};
void  ListenTask::Do(std::atomic<bool> &stopFlag) {
	while(!stopFlag){
		Buffer buf = client->connection->readMsg();
		std::shared_ptr<State> st= State::deserialize(buf);
		std::cout<< "state deserialized"<<std::endl;
		client->updateState(st);
		std::cout<<"got update from server"<<std::endl;
		client->callGuiUpdate(); 
	}
}
void ReactTask::Do(std::atomic<bool> &stopFlag) {;
	while(!stopFlag){
		if(client->checkState(stateId)){
			std::this_thread::sleep_for (std::chrono::milliseconds(50));
			continue;
		}
		stateId =  client->getState()->stateId;
		std::shared_ptr<Turn> t  = bst->Do(client->getState());
		std::cout<< "AI turn aX = " << t->aX << " aY = " << t->aY<<std::endl; 
		std::cout<<"turn made" <<std::endl;
		Buffer msg = t->serialize();
		client->connection->writeMsg(msg);
	}
}

void GuiTask::Do(std::atomic<bool> &stopFlag) {
	
}



int main(int argsize,char ** argv){
	static constexpr char LOCALHOST[] = "127.0.0.1";
    static constexpr int DEFAULT_PORT = 8111;
    int port = DEFAULT_PORT;
    if(argsize > 1) port = atoi(argv[1]);
    std::cout<<  "running on port " << port << std:: endl;	

    GameViewWindow w(argsize, argv, 1000, 700);
    std::this_thread::sleep_for (std::chrono::milliseconds(100));

	InternetAddress address = InternetAddress(LOCALHOST, port);
	Client client (address,1);
	try {
       client.runEventLoop();
   	} catch (const std::exception &ex) {
       std::cerr << ex.what() << std::endl;
       return 1;
   	}

   	State st;
   	int counter =0;
    while (counter < 300) {
        if(!w.isWinOpened()) break;
        std::this_thread::sleep_for (std::chrono::milliseconds(10));
        if(client.guiQueue->nonBlockingIsEmpty()){
        	++counter;
        	continue;
        }
        counter =0;
        std::shared_ptr<State> sp = client.guiQueue->wait_and_pop();
        //client.simulateState(0.010);
        //std::shared_ptr<State> sp = client.getState();
        if (!sp) continue;
        std::cout<<"here we are " << sp->stateId << std::endl;
        st = *sp.get();
        w.changeState(st);
    }
	return 0; 
}
