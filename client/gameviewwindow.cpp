#include "gameviewwindow.h"
#include "ballviewitem.h"
#include "coinviewitem.h"
#include "gameviewwindow.h"
#include <QApplication>
#include <QtWidgets>
#include <thread>
#include <iostream>
#include <chrono>
#include <atomic>
#include <map>
#include <set>

void GameViewWindow::startWindow(int argc, char *argv[], int w, int h) {
    QApplication a(argc, argv);

    QGraphicsScene scene;
    scene_ptr = &scene;
    scene.setItemIndexMethod(QGraphicsScene::NoIndex);

    QGraphicsView view(&scene);
    view.setRenderHint(QPainter::Antialiasing);
    view.setCacheMode(QGraphicsView::CacheBackground);
    view.setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    view.setDragMode(QGraphicsView::ScrollHandDrag);
    view.resize(w, h);
    view.setSceneRect(-1, -1, 1, 1);
    view.show();

    text_item = new ScoreViewItem();
    scene_ptr->addItem(text_item);

    QTimer timer;
    QObject::connect(&timer, SIGNAL(timeout()), &scene, SLOT(advance()));
    const int FPS = 60;
    timer.start(1000 / FPS);

    a.exec();
    win_opened = false;
    scene_ptr = 0;
}

GameViewWindow::GameViewWindow(int argc, char *argv[], int w, int h) {
    win_opened = true;
    scene_ptr = 0;
    arena = 0;
    text_item = 0;
    std::thread t(&GameViewWindow::startWindow, this, argc, argv, w, h);
    t.detach();
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
}

bool GameViewWindow::isWinOpened() {
    return win_opened;
}


std::map<id_t, BallViewItem*> players_map;
std::map<std::pair<float, float>, CoinViewItem*> coins_map;

void GameViewWindow::changeState(const State& state) {
    if(!scene_ptr) {
        return;
    }
    std::vector< std::shared_ptr<Player> > players = state.players;
    std::vector< std::shared_ptr<Coin> > coins = state.coins;
    float p_radius = state.playerRadius;
    float c_radius = state.coinRadius;

    std::set<id_t> players_id_set;
    for (int i = 0; i < players.size(); ++i) {
        players_id_set.insert(players[i]->id);
        if(players_map.count(players[i]->id)) {
            players_map[players[i]->id]->setX(players[i]->x);
            players_map[players[i]->id]->setY(players[i]->y);
        }
        else {
            BallViewItem *ball = new BallViewItem(players[i]->id, p_radius);
            ball->setX(players[i]->x);
            ball->setY(players[i]->y);
            scene_ptr->addItem(ball);
            players_map[players[i]->id] = ball;
        }
    }

    std::set<id_t> deleted_players;
    for(auto it = players_map.begin(); it != players_map.end(); ++it) {
        if(players_id_set.count(it->first) == 0) {
            BallViewItem *ball = it->second;
            scene_ptr->removeItem(ball);
            deleted_players.insert(it->first);
        }
    }

    for(auto it = deleted_players.begin(); it != deleted_players.end(); ++it) {
        BallViewItem *ball = players_map[*it];
        players_map.erase(*it);
        delete ball;
    }

    std::set<std::pair<float, float> > coins_id_set;
    for (int i = 0; i < coins.size(); ++i) {
        std::pair<float, float> key = std::make_pair(coins[i]->x, coins[i]->x);
        coins_id_set.insert(key);
        if(coins_map.count(key)) {
            coins_map[key]->setX(coins[i]->x);
            coins_map[key]->setY(coins[i]->y);
        }
        else {
            CoinViewItem *coin = new CoinViewItem(c_radius);
            coin->setX(coins[i]->x);
            coin->setY(coins[i]->y);
            scene_ptr->addItem(coin);
            coins_map[key] = coin;
        }
    }

    std::set<std::pair<float, float> > deleted_coins;
    for(auto it = coins_map.begin(); it != coins_map.end(); ++it) {
        if(coins_id_set.count(it->first) == 0) {
            CoinViewItem *coin = it->second;
            scene_ptr->removeItem(coin);
            deleted_coins.insert(it->first);
        }
    }

    for(auto it = deleted_coins.begin(); it != deleted_coins.end(); ++it) {
        CoinViewItem *coin = coins_map[*it];
        coins_map.erase(*it);
        delete coin;
    }


    if(!arena) {
        arena = new QGraphicsEllipseItem(-state.fieldRadius, -state.fieldRadius,
                                      2 * state.fieldRadius, 2 * state.fieldRadius);
        arena->setBrush(QBrush(Qt::NoBrush));
        scene_ptr->addItem(arena);
        arena->setPos(0, 0);
    }

    text_item->setScores(state);
    text_item->setX(-state.fieldRadius);
    text_item->setY(-state.fieldRadius);
}
