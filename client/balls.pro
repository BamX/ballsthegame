#-------------------------------------------------
#
# Project created by QtCreator 2014-05-25T15:36:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = balls
TEMPLATE = app


SOURCES += ballviewitem.cpp \
    client.cpp \
    coinviewitem.cpp \
    gameviewwindow.cpp \
    scoreviewitem.cpp \
    util_cpp.cpp

HEADERS  += ballviewitem.h \
    coinviewitem.h \
    gameviewwindow.h \
    scoreviewitem.h

CONFIG += c++11
