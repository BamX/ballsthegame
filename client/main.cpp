#include "gameviewwindow.h"
#include <QApplication>
#include <QtWidgets>
#include <iostream>
#include <chrono>
#include <thread>
#include <memory>

int main(int argc, char *argv[])
{
    GameViewWindow w(argc, argv, 1000, 700);
    std::this_thread::sleep_for (std::chrono::milliseconds(300));

    State st;
    st.playerRadius = 20;
    Player* p = new Player;
    p->x = qrand() % 500;
    p->y = qrand() % 500;
    p->id = 0;
    st.players.push_back(std::shared_ptr<Player>(p));

    st.coinRadius = 30;
    Coin* c = new Coin;
    c->x = qrand() % 500;
    c->y = qrand() % 500;
    st.coins.push_back(std::shared_ptr<Coin>(c));

    st.coinRadius = 30;
    Coin* cc = new Coin;
    cc->x = qrand() % 500;
    cc->y = qrand() % 500;

    // int iter = 0;
    while (true) {
        std::this_thread::sleep_for (std::chrono::milliseconds(100));
        if(!w.isWinOpened()) break;

        // if(iter < 90) {
        // if(qrand() % 2 == 0)
        //     st.players[0]->x+=5;
        // else
        //     st.players[0]->x-=5;
        // if(qrand() % 2 == 0)
        //     st.players[0]->y+=5;
        // else
        //     st.players[0]->y-=5;
        // }
        w.changeState(st);
        // ++iter;
        // if(iter == 50) {
        //     st.coins.clear();
        // }
        // if(iter == 70) {
        //     st.coins.push_back(std::shared_ptr<Coin>(cc));
        // }
        // if(iter == 90) {
        //     st.players.clear();
        // }
    }
}
