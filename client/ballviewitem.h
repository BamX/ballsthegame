#ifndef BALLVIEWITEM_H
#define BALLVIEWITEM_H

#include <QGraphicsItem>
#include <atomic>

class BallViewItem : public QGraphicsItem {
public:
    BallViewItem(int id, float radius = 10.0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    QRectF boundingRect() const;
    float getX();
    float getY();
    void setX(float nx);
    void setY(float ny);

protected:
    void advance(int step);

private:
    QColor color;
    float radius;
    std::atomic<float> new_x;
    std::atomic<float> new_y;
    int id;
};

#endif // BALLVIEWITEM_H
