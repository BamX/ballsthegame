#ifndef COINVIEWITEM_H
#define COINVIEWITEM_H

#include <QGraphicsItem>
#include <atomic>

class CoinViewItem : public QGraphicsItem {
public:
    CoinViewItem(float radius = 20.0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    QRectF boundingRect() const;
    float getX();
    float getY();
    void setX(float nx);
    void setY(float ny);

protected:
    void advance(int step);

private:
    QColor color;
    float radius;
    std::atomic<float> new_x;
    std::atomic<float> new_y;
};

#endif // COINVIEWITEM_H
