#include "scoreviewitem.h"
#include <iostream>

ScoreViewItem::ScoreViewItem() {
}

float ScoreViewItem::getX() {
    return new_x;
}

float ScoreViewItem::getY() {
    return new_y;
}

void ScoreViewItem::setX(float nx) {
    new_x = nx;
}

void ScoreViewItem::setY(float ny) {
    new_y = ny;
}
void ScoreViewItem::setScores(const State& state) {
    std::lock_guard<std::mutex> lock(text_mutex);
    text = "";
    for(auto p: state.players) {
        text += "Player " + QString::number(p->id) + ": ";
        text += "Score " + QString::number(p->score) + "\n";
    }
}

void ScoreViewItem::advance(int step) {
    if(!step) return;
    std::lock_guard<std::mutex> lock(text_mutex);

    setPlainText(text);
    setPos(new_x, new_y);
}

