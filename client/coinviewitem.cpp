#include "coinviewitem.h"

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

CoinViewItem::CoinViewItem(float radius) :
    radius(radius) {
}

QRectF CoinViewItem::boundingRect() const {
    return QRectF(-radius, -radius, 2 * radius, 2 * radius);
}

void CoinViewItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *) {
    painter->setBrush(QColor(255, 255, 0));
    painter->drawEllipse(-radius, -radius, 2 * radius, 2 * radius);

    painter->setBrush(QColor(255, 255, 128));
    painter->drawEllipse(-radius / 1.2, -radius / 1.2, 2 / 1.2 * radius, 2 / 1.2 * radius);
}

void CoinViewItem::advance(int step) {
    if(!step) return;

    setPos(new_x, new_y);
}

float CoinViewItem::getX() {
    return new_x;
}

float CoinViewItem::getY() {
    return new_y;
}

void CoinViewItem::setX(float nx) {
    new_x = nx;
}

void CoinViewItem::setY(float ny) {
    new_y = ny;
}
