#ifndef SCOREVIEWITEM_H
#define SCOREVIEWITEM_H

#include <QString>
#include <mutex>
#include <atomic>
#include <QGraphicsItem>
#include "../common/util.cpp"

class ScoreViewItem : public QGraphicsTextItem {
public:
    ScoreViewItem();
    float getX();
    float getY();
    void setX(float nx);
    void setY(float ny);
    void setScores(const State& state);

protected:
    void advance(int step);

private:
    std::mutex text_mutex;
    QString text;
    std::atomic<float> new_x;
    std::atomic<float> new_y;
};

#endif // SCOREVIEWITEM_H
